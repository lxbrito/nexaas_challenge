class SalesUploadsController < ApplicationController
  respond_to :html

  # GET /sales_uploads
  def index
    @sales_uploads = SalesUploadDecorator.decorate_collection(SalesUpload.last(15)) #TODO Pagination
  end

  # GET /sales_uploads/1
  def show
    @sales_upload = SalesUpload.find(params[:id]).decorate
  end

  # GET /sales_uploads/new
  def new
    @sales_upload = SalesUploadForm.new(SalesUpload.new)
  end

  # POST /sales_uploads
  def create
    @sales_upload = SalesUploadForm.new(SalesUpload.new)

    if @sales_upload.validate(sales_upload_params)
      @sales_upload.save
      FileParser.new(@sales_upload).call
      redirect_to @sales_upload, @sales_upload.model.decorate.message
    else
      render :new
    end
  end

  def error_file
    sales_upload = SalesUpload.find(params[:id])
    file_path = File.join(Rails.root, 'tmp', "erro_#{sales_upload.id}.txt")
    if sales_upload.processed_with_errors? && File.file?(file_path)
      send_file file_path, type: 'text/plain'
    else
      render "404.html"
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def sales_upload_params
    params.require(:sales_upload).permit(:name, :description, :file)
  end
end
