class SaleDecorator < Draper::Decorator
  delegate_all

  def formatted_total
    format_money(total)
  end

  def format_money(value)
    h.number_to_currency(value, unit: "R$", separator: ",", delimiter: ".", negative_format:"R$ (%n)")
  end

  def formatted_datetime
    I18n.l created_at, format: :simple
  end

  def formatted_status
    I18n.t("activerecord.attributes.sales_upload.status.#{status}")
  end

end
