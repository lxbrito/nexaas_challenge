class SalesUploadDecorator < Draper::Decorator
  delegate_all

  def formatted_total
    format_money(total)
  end

  def format_money(value)
    h.number_to_currency(value, unit: "R$", separator: ",", delimiter: ".", negative_format:"R$ (%n)")
  end

  def formatted_datetime
    I18n.l created_at, format: :simple
  end

  def formatted_status
    I18n.t("activerecord.attributes.sales_upload.status.#{status}")
  end

  def decorated_sales
    SaleDecorator.decorate_collection(sales)
  end

  def file_error_link
    return '' if !processed_with_errors?
    link_to
  end

  def message
    case status
      when 'ok'
        {notice:'O arquivo foi processado e as vendas persistidas'}
      when 'processed_with_errors'
        {alert:"O arquivo foi processado, mas houve erros\n Nenhuma venda foi persistida\n Consulte o arquivo com os erros"}
      else
        {alert:'O arquivo não pode ser processado'}
    end
  end

end
