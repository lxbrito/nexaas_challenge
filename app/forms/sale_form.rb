class SaleForm < Reform::Form
  property :customer
  property :description
  property :price
  property :quantity
  property :address
  property :supplier

  validates :customer, presence: true
  validates :description, presence: true
  validates :price, presence: true, numericality: {greater_than_or_equal_to: 0.01} #precision 1 cent
  validates :quantity, presence: true, numericality: {greater_than: 0, only_integer:true}
  validates :address, presence: true
  validates :supplier, presence: true

  validate :price do
    errors.add(:price, "Não são permitidos valores com mais de 2 casas decimais") unless price.to_s.match(/\A\d+(?:\.\d{0,2})?\z/)
  end
end