class SalesUploadForm < Reform::Form
  property :name
  property :description
  property :file, virtual:true

  validates :name, presence: true
  validates :description, presence: true
  validates :file, presence: true
end