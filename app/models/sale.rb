# == Schema Information
#
# Table name: sales
#
#  id              :integer          not null, primary key
#  sales_upload_id :integer
#  customer        :string           not null
#  description     :string           not null
#  price           :decimal(18, 2)   not null
#  quantity        :integer          not null
#  address         :string           not null
#  supplier        :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Sale < ActiveRecord::Base
  belongs_to :sales_upload

  def total
    price * quantity
  end
end
