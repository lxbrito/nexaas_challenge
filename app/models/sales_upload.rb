# == Schema Information
#
# Table name: sales_uploads
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  status      :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class SalesUpload < ActiveRecord::Base
  has_many :sales
  enum status:[:processing, :ok, :processed_with_errors, :unprocessable]

  def total
    #Warning this is subjected to N+1 queries, need to be replaced with SUM SQL
    sales.inject(BigDecimal.new(0)){|acc, sale| acc + sale.total}
  end
end
