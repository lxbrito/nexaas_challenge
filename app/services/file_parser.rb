require 'csv'
class FileParser
  HEADER_MAP= {"comprador"=>:customer, "descricao"=>:description, "preco-unitario"=>:price, "quantidade"=>:quantity,
               "endereco"=>:address, "fornecedor"=>:supplier}.freeze
  OPTIONS= { col_sep:"\t", headers:true, header_converters: lambda {|h| HEADER_MAP[h.parameterize]} }

  def initialize(file_upload)
    @file_upload = file_upload
    @has_errors = false
    @forms = []
  end

  def call
    read_csv
    if @has_errors
      return file_with_errors
    else
      return persist_sales
    end
  rescue
    @file_upload.model.unprocessable!
  end

  private
  def read_csv
    CSV.foreach(@file_upload.file.path, OPTIONS) do |row|
      form = SaleForm.new(Sale.new(sales_upload_id:@file_upload.model.id))
      @has_errors |= !form.validate(row.to_h)
      @forms << form
    end
  end

  def persist_sales
    ActiveRecord::Base.transaction do
      @forms.each(&:save)
      @file_upload.model.ok!
    end
  end

  def file_with_errors
    file_path = File.join(Rails.root, 'tmp', "erro_#{@file_upload.model.id}.txt")
    CSV.open(file_path, "w", {:col_sep => "\t"}) do |csv|
      csv << HEADER_MAP.keys.append("erros")
      attrs = HEADER_MAP.values
      @forms.each do |form|
        csv << attrs.map{|attr| form.send(attr)}.append(format_errors(form.errors))
      end
    end
    @file_upload.model.processed_with_errors!
    #return file
  end

  def format_errors(errors)
    errors.map do |k,v|
      key = HEADER_MAP.key(k)
      Array(v).map{|msg| "#{key} #{msg}"}.join(', ')
    end.join(', ')
  end
end