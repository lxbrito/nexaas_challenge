class CreateSalesUploads < ActiveRecord::Migration
  def change
    create_table :sales_uploads do |t|
      t.string :name
      t.string :description
      t.integer :status, null: false, default:0

      t.timestamps null: false
    end
  end
end
