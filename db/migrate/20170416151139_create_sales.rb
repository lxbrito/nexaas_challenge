class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.references :sales_upload, index: true, foreign_key: true
      t.string :customer, null: false
      t.string :description, null: false
      t.decimal :price, null: false, precision: 18, scale: 2
      t.integer :quantity, null: false
      t.string :address, null: false
      t.string :supplier, null: false

      t.timestamps null: false
    end
  end
end
