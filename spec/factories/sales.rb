# == Schema Information
#
# Table name: sales
#
#  id              :integer          not null, primary key
#  sales_upload_id :integer
#  customer        :string           not null
#  description     :string           not null
#  price           :decimal(18, 2)   not null
#  quantity        :integer          not null
#  address         :string           not null
#  supplier        :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :sale do
    customer { Faker::Name.name }
    description { Faker::Commerce.product_name }
    price { Faker::Commerce.price }
    quantity { rand(25)}
    address { Faker::Address.street_address }
    supplier { Faker::Company.name }
    association :sales_upload
  end
end
