# == Schema Information
#
# Table name: sales_uploads
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  status      :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :sales_upload do
    name { Faker::File.file_name('',nil,'txt','') }
    description { Faker::Lorem.sentence }
    status 1

    trait :with_sales do
      after(:create) do |sales_upload, evaluator|
        create(:sale, sales_upload: sales_upload)
        create(:sale, sales_upload: sales_upload)
      end
    end
  end

  factory :sales_upload_error, parent: 'sales_upload' do
    status 2
  end
end
