require 'rails_helper'

RSpec.describe SaleForm, type: :model do
  let(:valid_object) { FactoryGirl.build(:sale) }
  subject { SaleForm.new(valid_object) }

  it { is_expected.to validate_presence_of(:customer) }
  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:price) }
  it { is_expected.to validate_presence_of(:quantity) }
  it { is_expected.to validate_presence_of(:address) }
  it { is_expected.to validate_presence_of(:supplier) }


  it { is_expected.to validate_numericality_of(:price) }
  it { is_expected.to validate_numericality_of(:quantity) }

  it "validates max two decimal places on price" do
    subject.validate(price: 1.111)
    expect(subject.errors[:price]).to match_array(["Não são permitidos valores com mais de 2 casas decimais"])
  end

  it "validates price being greater or equal 0.01" do
    subject.validate(price: 0)
    expect(subject.errors[:price]).to match_array(["deve ser maior ou igual a 0.01"])
  end

  it "passes price being equal 0.01" do
    subject.validate(price: 0.01)
    expect(subject.errors[:price]).to be_empty
  end

  it "validates quantity being integer" do
    subject.validate(quantity: 0.1)
    expect(subject.errors[:quantity]).to match_array(["não é um número inteiro"])
  end

  it "validates quantity being greater than 0" do
    subject.validate(quantity: 0)
    expect(subject.errors[:quantity]).to match_array(["deve ser maior que 0"])
  end
end
