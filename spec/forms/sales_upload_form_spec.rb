require 'rails_helper'

RSpec.describe SalesUploadForm, type: :model do
  let(:valid_object) { FactoryGirl.build(:sales_upload) }
  subject { SalesUploadForm.new(valid_object) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:file) }

end
