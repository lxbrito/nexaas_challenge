# == Schema Information
#
# Table name: sales
#
#  id              :integer          not null, primary key
#  sales_upload_id :integer
#  customer        :string           not null
#  description     :string           not null
#  price           :decimal(18, 2)   not null
#  quantity        :integer          not null
#  address         :string           not null
#  supplier        :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe Sale, type: :model do
  it "Calculates the total" do
    sale = FactoryGirl.build(:sale, quantity:3, price:2.11)
    expect(sale.total).to eq 6.33
  end
end
