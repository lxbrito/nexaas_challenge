# == Schema Information
#
# Table name: sales_uploads
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  status      :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe SalesUpload, type: :model do
  it "Calculates the total" do
    sale_upload = FactoryGirl.create(:sales_upload, :with_sales)
    tt = sale_upload.sales[0].total + sale_upload.sales[1].total
    expect(sale_upload.total).to eq tt
  end
end
