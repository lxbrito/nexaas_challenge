require 'rails_helper'

RSpec.describe "sales_uploads/index", type: :view do
  before(:each) do
    @sales_uploads = assign(:sales_upload, [
        FactoryGirl.create(:sales_upload, :with_sales).decorate,
        FactoryGirl.create(:sales_upload_error).decorate
    ])
  end

  it "renders a list of sales_uploads" do
    render
    assert_select "tr>td", :text => @sales_uploads[0].name, :count => 1
    assert_select "tr>td", :text => @sales_uploads[1].name, :count => 1
    assert_select "tr>td", :text => @sales_uploads[0].description, :count => 1
    assert_select "tr>td", :text => @sales_uploads[1].description, :count => 1
    assert_select "tr>td", :text => @sales_uploads[0].formatted_total, :count => 1
    assert_select "tr>td", :text => 'R$ 0,00', :count => 1
    assert_select "tr>td", :text => @sales_uploads[0].formatted_datetime, :count => 2 #bad test, may fail
  end
end
