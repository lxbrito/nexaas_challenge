require 'rails_helper'

RSpec.describe "sales_uploads/new", type: :view do
  before(:each) do
    assign(:sales_upload, SalesUpload.new(
      :name => "MyString",
      :description => "MyString",
      :status => 1
    ))
  end

  it "renders new sales_upload form" do
    render

    assert_select "form[action=?][method=?]", sales_uploads_path, "post" do

      assert_select "input#sales_upload_name[name=?]", "sales_upload[name]"

      assert_select "input#sales_upload_description[name=?]", "sales_upload[description]"

      assert_select "input#sales_upload_file[name=?]", "sales_upload[file]"

      assert_select "input#sales_upload_file[type=?]", "file"
    end
  end
end
