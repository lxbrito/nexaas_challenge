require 'rails_helper'

RSpec.describe "sales_uploads/show", type: :view do
  before(:each) do
    @sales_upload = assign(:sales_upload, FactoryGirl.create(:sales_upload, :with_sales).decorate)
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Nome/)
    expect(rendered).to match(/Descrição/)
    expect(rendered).to match(/Data/)
    expect(rendered).to match("#{@sales_upload.formatted_datetime}")
    expect(rendered).to match("#{@sales_upload.name}")
    expect(rendered).to match("#{@sales_upload.description}")
    expect(rendered).to match("#{@sales_upload.formatted_status}")

    assert_select "tfooter>tr>th", :text => "Total", :count => 1
    assert_select "tfooter>tr>th", :text => @sales_upload.formatted_total, :count => 1
  end
end
